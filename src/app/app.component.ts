import { Component } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  private releaseDate = new Date('Jul 2, 2020').getTime();
  private users$: AngularFirestoreCollection<any>;
  email: string = null;
  userName: string;
  note: string;
  quoteList1 = [
    "\“It is what difference we’ve made in the lives of others, that will determine the significance of the life we lead.” - Nelson Mandella",
    "\"Wander...and you may uncover something that totally ends up changing your life.\" - Q",
    "\"Its ordinary to love the beautiful, its beautiful to love the ordinary.\" - Unknown",
    "\"True education is replacing an empty mind with an open one.\" - Malcolm Forbes",
    "\"Our greatest glory is not in never falling, but in rising every time we fall.\" - Confucius",
    "\"Where there is life, there is hope.\"",
  ];

  quoteList2 = [
    "\"It is up to us whether our eyes and ears hinder the intake of knowledge or enrich it.\" - Q",
    "\"It’s not because things are difficult that we do not dare, it is because we do not dare that things get difficult.\" - Seneca",
    "\“If you want to go fast, go alone...if you want to go far, go together\” - Nelson Mandella",
    "\"We\'re not analytical beings given a beat, we\'re emotional beings given a brain.\" - Q",
    "\"Difficulties are meant to rouse, not discourage. The human spirit is to grow strong by conflict.\" - Willian Ellery"
  ];
  quote1: string;
  quote2: string;
  daysRemaining: string;
  hoursRemaining: string;
  features = [
    {
      icon: "fas fa-fire",
      headline: "Future Ready",
      description: "The stories are never ending: one more coming every week.",
    },
    {
      icon: "fas fa-palette",
      headline: "Unique Design",
      description: "Made with thinking of you at every step.",
    },
    {
      icon: "far fa-lightbulb",
      headline: "Limitless",
      description:
        "Functionality to edit, write and share content without any limits.",
    },
    {
      icon: "far fa-chart-bar",
      headline: "Real-time Tracking",
      description: "Analyze your content. Every step. The entire way.",
    },
  ];

  constructor(public firestore: AngularFirestore) {
    this.users$ = this.firestore.collection("beta_users");
    this.quote1 = this.quoteList1[0];
    this.quote2 = this.quoteList2[0];

    setInterval(() => {
      this.calculateTime();
    }, 1000);

    setInterval(() => {
      this.updateQuotes();
    }, 5000);
  }

  private updateQuotes() {
    this.quote1 = this.quoteList1[
      Math.floor(Math.random() * this.quoteList1.length)
    ];
    this.quote2 = this.quoteList2[
      Math.floor(Math.random() * this.quoteList2.length)
    ];
  }

  public addUser() {
    this.email = this.email.toLowerCase();
    const existingUser = this.users$.doc(this.email);
    existingUser.get().subscribe((snapshot) => {
      if (snapshot.exists) {
        alert("User already exists");
      } else {
        this.users$
          .doc(this.email)
          .set({
            email: this.email,
            name: this.userName || "",
            note: this.note || "",
          })
          .then(() => {
            alert('Thank you for signing up to be our BETA tester!');
          });
      }
      this.email = this.userName = this.note = null;
    });
  }

  private calculateTime() {
    const now = new Date().getTime();
    const timeleft = this.releaseDate - now;
    this.daysRemaining = Math.floor(
      timeleft / (1000 * 60 * 60 * 24)
    ).toString();
    this.hoursRemaining = Math.floor(
      (timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    ).toString();
  }
}
