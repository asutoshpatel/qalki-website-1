'use strict';

const functions = require('firebase-functions');
const nodemailer = require('nodemailer');

const gmailEmail = functions.config().qalki.email;
const gmailPassword = functions.config().qalki.email_password;
const mailTransport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'qalkiofficial@gmail.com',
        pass: 'popatbobatea',
    },
});

exports.sendEmail = functions.firestore
    .document('contact_us/{contact_id}')
    .onCreate((snap, context) => sendContactEmail(snap));


async function sendContactEmail(snap) {
    const mailOptions = {
        from: snap.data().email,
        to: functions.config().qalki.email,
        subject: 'Contact Form Message',
        html: `<h1>Contact Form Message</h1>
                        <p><b>Email: </b>${snap.data().message}<br></p>`
    };

    await mailTransport.sendMail(mailOptions);
    return null;
}
